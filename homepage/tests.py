from django.test import TestCase, Client
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class HomeTest(TestCase):
    def test_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

class HomeFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')		
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(HomeFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(HomeFunctionalTest, self).tearDown()
		
    def test_title_page(self):
        self.selenium.get('http://127.0.0.1:8000/')
        self.assertEqual(self.selenium.title, "home")

    def test_if_search_clicked_show_content(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        searchbox = selenium.find_element_by_css_selector(".searchbox")
        searchbox.send_keys("Liverpool")

        time.sleep(2)
        
        btnSearch = selenium.find_element_by_css_selector(".btn-search")
        btnSearch.click()

        time.sleep(2)
        self.assertIn('Titanic and Liverpool', selenium.page_source)
        time.sleep(2)